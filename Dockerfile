FROM python:latest
RUN apt-get -y update && apt-get install -y python3 python3-pip git
# RUN apt-get -y upgrade
RUN apt-get -y autoremove && apt-get -y autoclean

WORKDIR /usr/src/app
RUN chmod 777 /usr/src/app
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .

CMD ["bash", "start.sh"]
